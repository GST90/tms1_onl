import itertools

# input = input("Enter your phrase: ")
# alphabet = "abcdefghijklmnopqrstuvwxyz"


def count_letters(input: str):
    # Convert input to list
    input_to_list = []
    input_to_list[:0] = input

    # Count repeated letters in input_to_list with itertools
    result = [(x, len(list(y))) for x, y in itertools.groupby(input_to_list)]

    # Convert result back to string
    result_str = str(result)

    # Leave only letters and numbers
    result_alphanum = ''.join([i for i in result_str if i.isalnum()])

    # Remove "1" symbols
    result_wo_1 = (result_alphanum.translate({ord('1'): None}))

    return result_wo_1


# print(count_letters(input))
