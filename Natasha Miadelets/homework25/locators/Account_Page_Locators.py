from selenium.webdriver.common.by import By


class AccountPageLocators:

    LOCATOR_MY_ACCOUNT_TEXT = (By.XPATH, "//h1[text()='My account']")
    LOCATOR_CART_EMPTY = (By.XPATH, "//span[text()='(empty)']")
