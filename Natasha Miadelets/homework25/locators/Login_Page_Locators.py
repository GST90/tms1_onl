from selenium.webdriver.common.by import By


class LoginPageLocators:

    EMAIL_FIELD = (By.XPATH, "//input[@id='email']")
    PASSWORD_FIELD = (By.XPATH, "//input[@id='passwd']")
    SIGN_IN_BUTTON = (By.XPATH, "//i[@class='icon-lock left']")
    LOCATOR_CREATE_ACCOUNT_TEXT = (By.XPATH,
                                   "//h3[text()='Create an account']")
    LOCATOR_CREATE_ACCOUNT_FIELD = (By.XPATH, "//input[@name='email_create']")
    LOCATOR_CREATE_ACCOUNT_BUTTON = (By.XPATH, "//button[@id='SubmitCreate']")
    LOCATOR_REGISTER_BUTTON = (By.XPATH, "//span[text()='Register']")
