import xml.etree.cElementTree as ET
root = ET.parse("library.xml").getroot()


def search_by_author(root, attribute: str, value: str):

    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute and \
                    attrib_child.text.find(value) >= 0:
                print(f'{attrib_child.tag} of book - {attrib_child.text};'
                      f' {child.tag} is {child.attrib}')


def search_by_price(root, attribute: str, value: str):

    for child in root:
        for attrib_child in list(child):
            if attribute == attrib_child.tag and value == attrib_child.text:
                print(f'{attrib_child.tag} of book - {attrib_child.text};'
                      f' {child.tag} is {child.attrib}')


def search_by_title(root, attribute: str, value: str):

    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute and value == attrib_child.text:
                print(f'{attrib_child.tag} of book - {attrib_child.text};'
                      f' {child.tag} is {child.attrib}')


def search_by_description(root, attribute: str):

    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute:
                print(f'{attrib_child.tag} of book - {attrib_child.text};'
                      f' {child.tag} is {child.attrib}')


search_by_author(root, 'author', 'Martin')
search_by_price(root, 'price', '17.89')
search_by_title(root, 'title', 'Midnight Rain')
search_by_description(root, 'description')
