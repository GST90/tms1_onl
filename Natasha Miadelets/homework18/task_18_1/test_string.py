from unittest import TestCase
from unittest import expectedFailure
from main_string import String


class TestString(TestCase):

    def setUp(self):
        self.number_1 = "aacccchhhhheebbbc"
        self.number_2 = "aaajjjffhfdjkj"
        self.number_3 = "ffeeerrrree"
        self.answer_1 = "a2c4h5e2b3c"
        self.answer_2 = "a3j3f2hfdjkj"
        self.answer_3 = "f2e3r4e2"
        self.answer_wrong = "f2e5r4"
        self.string = String()

    def test_1(self):
        self.assertEqual(self.string.count_letters(self.number_1),
                         self.answer_1)

    def test_2(self):
        self.assertEqual(self.string.count_letters(self.number_2),
                         self.answer_2)

    def test_3(self):
        self.assertEqual(self.string.count_letters(self.number_3),
                         self.answer_3)

    @expectedFailure
    def test_wrong_answer(self):
        self.assertEqual(self.string.count_letters(self.number_3),
                         self.answer_wrong)
