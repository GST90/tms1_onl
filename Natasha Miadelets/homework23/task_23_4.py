from selenium import webdriver
from time import sleep

url = "https://ultimateqa.com/filling-out-forms/"
browser = webdriver.Chrome("./chromedriver 2")
message = "It is message"
message_field_xpath = "//textarea[@id=\"et_pb_contact_message_0\"]"
submit_button_xpath = "//div[@class=\"et_contact_bottom_container\"]/button"
text1_xpath = "//p[text()=\"Please, fill in the following fields:\"]"
text2_xpath = "//li[text()=\"Name\"]"
browser.get(url)
# Заполняю поле
message_input = browser.find_element_by_xpath(message_field_xpath)
message_input.send_keys(message)

submit_button = browser.find_element_by_xpath(submit_button_xpath)
submit_button.click()
# Жду пока появится сообщение и ищу его текст
sleep(3)
text1 = browser.find_element_by_xpath(text1_xpath).text
text2 = browser.find_element_by_xpath(text2_xpath).text
assert text1 == "Please, fill in the following fields:",\
    f'Please refresh the page and try again. is not equal {text1}'
assert text2 == "Name", f'Please refresh the page and try again.' \
                        f' is not equal {text2}'
browser.quit()
