from selenium import webdriver
from time import sleep

url = "https://ultimateqa.com/filling-out-forms/"
browser = webdriver.Chrome("./chromedriver 2")
name = "Natasha"
name_field_xpath = "//input[@id=\"et_pb_contact_name_0\"]"
submit_button_xpath = "//div[@class=\"et_contact_bottom_container\"]/button"
text1_xpath = "//p[text()=\"Please, fill in the following fields:\"]"
text2_xpath = "//li[text()=\"Message\"]"
browser.get(url)
# Заполняю поле
name_input = browser.find_element_by_xpath(name_field_xpath)
name_input.send_keys(name)

submit_button = browser.find_element_by_xpath(submit_button_xpath)
submit_button.click()
# Жду пока появится сообщение и ищу его текст
sleep(3)
text1 = browser.find_element_by_xpath(text1_xpath).text
text2 = browser.find_element_by_xpath(text2_xpath).text
assert text1 == "Please, fill in the following fields:",\
    f'Please refresh the page and try again. is not equal {text1}'
assert text2 == "Message", f'Please refresh the page and try again.' \
                           f' is not equal {text2}'
browser.quit()
