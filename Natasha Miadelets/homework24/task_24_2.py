from selenium import webdriver

url = "http://the-internet.herokuapp.com/frames"
browser = webdriver.Chrome("./chromedriver 2")
browser.implicitly_wait(5)
browser.get(url)

link_xpath = "//a[text()='iFrame']"
text_xpath = "//p[text()='Your content goes here.']"
iframe_xpath = "//iframe[@id='mce_0_ifr']"
link = browser.find_element_by_xpath(link_xpath)
link.click()

browser.switch_to.frame(browser.find_element_by_xpath(iframe_xpath))
text = browser.find_element_by_xpath(text_xpath).text

assert text == 'Your content goes here.',\
    f'Your content goes here. is not equal {text}'

browser.quit()
