import json

with open('students.json', 'r') as f:
    students_data = json.load(f)


def search(param, value):
    """Search by Club, Class or other parameter"""
    students_list = []
    for i in students_data:
        if i[param] == value:
            students_list.append(i["Name"])
    return students_list


def filter_by_gender(param, value):
    """Filter students by gender"""
    students_list = []
    for i in students_data:
        if i[param] == value:
            students_list.append(i["Name"])
    if value == "W":
        return f"Women: {students_list}"
    else:
        return f"Men: {students_list}"


def search_by_name(name):
    """Search student by Name"""
    for i in students_data:
        if name in i["Name"]:
            return i


print(search("Class", "1a"))
print(search("Club", "box"))
print(filter_by_gender("Gender", "W"))
print(search_by_name("S"))
