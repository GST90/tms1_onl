# Calculator

def calculation(selected_operation):
    """This function calculates entered numbers"""
    if selected_operation == "Addition":
        print("You selected Addition")
        num1 = int(input("Enter first number: "))
        num2 = int(input("Enter first number: "))
        sum_num = num1 + num2
        print(f"Result: {sum_num}")
    elif selected_operation == "Subtraction":
        print("You selected Subtraction")
        num1 = int(input("Enter minuend number: "))
        num2 = int(input("Enter subtrahend number: "))
        subt_num = num1 - num2
        print(f"Result: {subt_num}")
    elif selected_operation == "Multiplication":
        print("You selected Multiplication")
        num1 = int(input("Enter first number: "))
        num2 = int(input("Enter second number: "))
        mult_num = num1 * num2
        print(f"Result: {mult_num}")
    elif selected_operation == "Division":
        print("You selected Division")
        num1 = int(input("Enter first number: "))
        num2 = int(input("Enter second number: "))
        while num2 == 0:
            num2 = int(input("Cannot divide by zero. Enter new number: "))
        div_num = str(round(num1 / num2, 1))
        result = div_num.split('.')
        print(f"Quotient: {result[0]}, Remainder: {result[1]}")


def operation(select):
    """This function defines, which operation is selected"""
    if select == "1":
        return calculation("Addition")
    elif select == "2":
        return calculation("Subtraction")
    elif select == "3":
        return calculation("Multiplication")
    elif select == "4":
        return calculation("Division")


# Operation selection by user
oper = input("Select operation:\n"
             "1. Addition\n"
             "2. Subtraction\n"
             "3. Multiplication\n"
             "4. Division\n"
             "Number of needed operation: ")
operation(oper)
