# Library
class Book:

    def __init__(self, book_title: str, author: str, pg_quantity: int,
                 isbn: str, booked=None, in_reading=False):
        """This method allows to create an instance in class Book"""
        self.book_title = book_title
        self.author = author
        self.pg_quantity = pg_quantity
        self.isbn = isbn
        if booked is None:
            booked = []
        self.booked = booked
        self.in_reading = in_reading

    def get_book_info(self):
        """This method gives an info about books"""
        print(f"Title: {self.book_title}, Author: {self.author}, "
              f"number of pages: {self.pg_quantity}, ISBN: {self.isbn}")


class Reader:

    def __init__(self, name: str, user_id: str, booked_book=None,
                 is_reading=None):
        """This method allows to create a reader in the library"""
        if booked_book is None:
            booked_book = []
        if is_reading is None:
            is_reading = []
        self.name = name
        self.__user_id = user_id
        self.booked_book = booked_book
        self.is_reading = is_reading

    def take_a_book(self, book):
        """This method allows reader to take a book if it's not reading
        now. Otherwise it checks is this book reserved and validates is
        reader in the queue and his/her position"""
        if book.in_reading:
            print(f"Sorry, you cannot take {book.book_title} to read")
        elif not book.in_reading and len(book.booked) != 0:
            if self.__user_id in book.booked[0]:
                book.in_reading = True
                book.booked.remove(self.__user_id)
                self.is_reading.append([book.book_title, book.isbn])
                print(f"Don't forget return {book.book_title} by "
                      f"{book.author} in 30 days")
            elif self.__user_id in book.booked and self.__user_id \
                    not in book.booked[0]:
                print("Sorry, You are not next in the queue")
        else:
            book.in_reading = True
            self.is_reading.append([book.book_title, book.isbn])
            print(f"Don't forget return {book.book_title} by {book.author}"
                  f" in 30 days")

    def return_book(self, book):
        """This method allows reader to return book in the library"""
        if book.in_reading:
            book.in_reading = False
            self.is_reading.remove([book.book_title, book.isbn])
            print("Thank You for returning book! Want to take another one?")
        else:
            print("This book not from our library")

    def reserve_book(self, book):
        """This method allows reader to reserve books and gives information
        about his/her position in the queue"""
        if not book.in_reading:
            print("You can take this book now!")
        elif book.in_reading:
            book.booked.append(self.__user_id)
            if len(book.booked) == 1:
                print(f"You are next to read {book.book_title} by "
                      f"{book.author}.")
            else:
                print(f"Your number is {len(book.booked)} in the queue"
                      f" for this book.")


book_1 = Book('Treasure Island', 'Robert Louis Stevenson', 292,
              '9780451530974')
book_2 = Book('Harry Potter and the Goblet of Fire', 'J. K. Rowling', 636,
              '9781408855683')
book_3 = Book('1984', 'George Orwell', 328, '9780451524935')
book_4 = Book('Career of Evil', 'Robert Galbraith (J. K. Rowling)', 512,
              '9780751571417')
reader_1 = Reader('Victoria', '001')
reader_2 = Reader('Alex', '002')
reader_3 = Reader('JD', '003')

reader_1.take_a_book(book_2)
reader_1.take_a_book(book_4)
reader_1.take_a_book(book_3)
reader_1.return_book(book_4)
reader_3.reserve_book(book_2)
reader_3.reserve_book(book_3)
book_4.get_book_info()
