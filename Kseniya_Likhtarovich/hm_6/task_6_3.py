"""
Реализуйте программу c запросом номера операции и двух чисел.

Программа выводит результат, есть проверка деления на 0.
"""


def calculation(operation, f_num, s_num):
    """Производит вычисления."""
    if operation == 1:
        return f_num + s_num
    elif operation == 2:
        return f_num - s_num
    elif operation == 3:
        return f_num * s_num
    elif operation == 4:
        if s_num != 0:
            return f"Частное: {f_num // s_num}, Остаток: {f_num % s_num}"
        else:
            return "Деление на ноль!"


print("Выберите операцию: 1. Сложение 2. Вычитание "
      "3. Умножение 4. Деление")
operations = int(input("Введите номер пункта меню: "))
first_num = int(input("Введите первое число: "))
second_num = int(input("Введите второе число: "))


print(calculation(operations, first_num, second_num))
