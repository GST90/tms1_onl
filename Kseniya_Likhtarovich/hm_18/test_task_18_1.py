import unittest
from main_task_18_1 import word


class TestString(unittest.TestCase):

    def test_str_1(self):
        self.assertEqual(word("aaabbvb"), "a3b2vb")

    def test_str_2(self):
        self.assertEqual(word("abeehhhhhccced"), "abe2h5c3ed")

    def test_str_3(self):
        self.assertEqual(word("abcde"), "abcde")

    def test_str_4(self):
        self.assertEqual(word("aaaaa"), "a5")

    def test_str_5(self):
        self.assertEqual(word("11122233336"), "1323346")

    def test_str_6(self):
        self.assertEqual(word("!!###&&&*^"), "!2#3&3*^")

    @unittest.expectedFailure
    def test_str_7(self):
        self.assertEqual(word("aaaaa"), "a")

    @unittest.expectedFailure
    def test_str_8(self):
        self.assertEqual(word("aaabbvb"), "a3b3v")


if __name__ == "__main__":
    unittest.main()
