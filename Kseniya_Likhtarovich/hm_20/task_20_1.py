from selenium import webdriver


def test_wiki_ui():
    url = "https://en.wikipedia.org/"
    title = "Python (programming language)"

    driver = webdriver.Chrome()
    driver.get(url)

    search = driver.find_element_by_id("searchInput")
    search.send_keys(title)
    search.submit()

    heading = driver.find_element_by_id("firstHeading").text
    assert title == heading, f"{heading} != {title}"

    driver.quit()
