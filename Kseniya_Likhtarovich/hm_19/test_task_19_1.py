import pytest
from main_task_19_1 import word


class TestString:

    @pytest.mark.smoke
    @pytest.mark.parametrize("param, result", [("aaabbvb", "a3b2vb"),
                                               ("abcde", "abcde"),
                                               ("aaaaa", "a5")])
    def test_str_1(self, param, result):
        assert word(param) == result

    @pytest.mark.regression
    @pytest.mark.parametrize("param, result",
                             [("abeehhhhhccced", "abe2h5c3ed"),
                              ("11122233336", "1323346"),
                              ("!!###&&&*^", "!2#3&3*^")])
    def test_str_2(self, param, result):
        assert word(param) == result

    @pytest.mark.skip
    def test_str_3(self):
        assert word("aaaaa") != "a"

    @pytest.mark.daily
    @pytest.mark.xfail(reason="Баг TMS-1034")
    def test_str_4(self):
        assert word("aaabbvbv") == "a3b3v", "Должно быть a3b2vbv"
