from dina_shunkina.hm_19.main_1 import func
import pytest


@pytest.fixture(scope="session")
def start_work():
    print("Let's check our tests!")


@pytest.fixture
def variable():
    return "abeehhhhhccced"


@pytest.mark.positive
def test_positive(start_work, variable):
    assert func(variable) == "abe2h5c3ed", "There is not a mistake"


@pytest.mark.negative
@pytest.mark.xfail
def test_negative(start_work, variable):
    assert func(variable) == "abe2h5c3", "There is a mistake"


@pytest.mark.parametrize('var', ["abe"])
def test_positive_2(var):
    assert func(var) == var, "There is not a mistake"
