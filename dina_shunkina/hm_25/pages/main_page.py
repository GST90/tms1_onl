from dina_shunkina.hm_25.pages.base_page import BasePage
from dina_shunkina.hm_25.locators.main_page_locator import MainPageLocator


class MainPage(BasePage):
    def should_be_signin_page(self):
        self.visibility_element(MainPageLocator.LOGO_TXT)

    def open_signin_page(self):
        signin_button = self.find_element(MainPageLocator.SIGNIN_BUTTON)
        signin_button.click()

    def search_item(self, item: str):
        search_field = self.find_element(MainPageLocator.LOCATOR_SEARCH_FIELD)
        search_field.send_keys(item)
        search_but = self.find_element(MainPageLocator.LOCATOR_SEARCH_BUTTON)
        search_but.click()

    def check_empty_cart(self):
        cart_but = self.find_element(MainPageLocator.LOCATOR_CART_BUTTON).text
        assert cart_but == "(empty)"
