from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep


def test_login():
    url = "https://ultimateqa.com/filling-out-forms/ "
    path = "C:/chromedriver.exe"
    driver = webdriver.Chrome(path)
    driver.get(url)
    Name = "Test_User"
    Name_input = driver.find_element(By.ID, "et_pb_contact_name_0")
    Name_input.send_keys(Name)
    button_css_selector = driver.find_element(
        By.XPATH, '//*[@id="et_pb_contact_form_0"]//button')
    button_css_selector.click()
    contact_message = driver.find_element(
        By.XPATH, '//*[@id="et_pb_contact_form_0"]//p').text
    contact_message2 = driver.find_element(
        By.XPATH,
        "//*[@id='et_pb_contact_form_0']"
        "//div[1]//ul[1]/li[contains(., 'Message')]").text
    assert "Please, fill in the following fields:" == contact_message
    assert "Message" == contact_message2
    sleep(2)
