def test_success_form(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    browser.find_element_by_id(
        "et_pb_contact_name_0").send_keys("test")
    browser.find_element_by_id(
        "et_pb_contact_message_0").send_keys("test")
    browser.find_element_by_xpath(
        "//div[@id='et_pb_contact_form_0']//button").click()

    assert browser.find_element_by_xpath(
        "//div[@class='et-pb-contact-"
        "message']/p").text == "Thanks for contacting us"


def test_fail_form_without_name(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    browser.find_element_by_id(
        "et_pb_contact_message_0").send_keys("test")
    browser.find_element_by_xpath(
        "//div[@id='et_pb_contact_form_0']//button").click()

    assert browser.find_element_by_xpath(
        "//div[@id='et_pb_contact_form_0']"
        "//li").text == "Name"


def test_fail_form_without_msg(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    browser.find_element_by_id(
        "et_pb_contact_name_0").send_keys("test")
    browser.find_element_by_xpath(
        "//div[@id='et_pb_contact_form_0']//button").click()

    assert browser.find_element_by_xpath(
        "//div[@id='et_pb_contact_form_0']"
        "//li").text == "Message"
