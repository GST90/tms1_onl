from Rinat_Amirzhanov.homework25.pages.cart_page import CartPageHelper
from Rinat_Amirzhanov.homework25.pages.main_page import MainPageHelper


def test_empty_cart(browser):
    main_page = MainPageHelper(browser)
    main_page.open_main_page()
    main_page.open_cart_page()
    cart_page = CartPageHelper(browser)
    cart_page.should_be_cart_page()
    cart_page.check_cart_empty()
