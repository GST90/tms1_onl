ang = ' abcdefghijklmnopqrstuvwxyz'
rus = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'


def encode():
    s = input('Введите текст: ').strip().lower()
    b = set(s[0]) & set(ang)
    if len(b) >= 1:
        n = int(input('Введите шаг: '))
        res = ''
        for c in s:
            res += ang[(ang.index(c) + n) % len(ang)]
        print('Результат: "' + res + '"')
    else:
        n = int(input('Введите шаг: '))
        res = ''
        for c in s:
            res += rus[(rus.index(c) + n) % len(rus)]
        print('Результат: "' + res + '"')


def decode():
    s = input('Введите текст: ').strip().lower()
    b = set(s[0]) & set(ang)
    if len(b) >= 1:
        n = int(input('Введите шаг: '))
        res = ''
        for c in s:
            res += ang[(ang.index(c) - n) % len(ang)]
        print('Результат: "' + res + '"')
    else:
        n = int(input('Введите шаг: '))
        res = ''
        for c in s:
            res += rus[(rus.index(c) - n) % len(rus)]
        print('Результат: "' + res + '"')


def code():
    v = input('Что вы хотите сделать? \n 1. Зашифровать \n 2. Расшифровать \n')
    if v == 1:
        encode()
    else:
        decode()


# encode()
# decode()
code()
