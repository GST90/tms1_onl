from unittest import TestCase
from unittest import expectedFailure
from main import Game


class TestGame(TestCase):

    def setUp(self):
        self.number_1 = "1413"
        self.number_2 = "4444"
        self.number_3 = "1314"
        self.number_4 = "1111"
        self.number_5 = "1212"
        self.number_6 = "3333"
        self.answer_1 = "Вы выиграли!"
        self.answer_2 = '3 Коровы', '1 Быки'
        self.answer_3 = '2 Коровы', '2 Быки'
        self.answer_4 = '2 Коровы', '2 Быки'
        self.answer_5 = '0 Коровы', '2 Быки'
        self.answer_6 = '3 Коровы', '1 Быки'
        self.game = Game()

    def test_bulls_4(self):
        self.assertEqual(self.game.bulls(self.number_1),
                         self.answer_1)

    def test_cows_0_bulls_2(self):
        self.assertEqual(self.game.bulls(self.number_2),
                         self.answer_2)

    def test_cows_1_bulls_2(self):
        self.assertEqual(self.game.bulls(self.number_3),
                         self.answer_3)

    def test_cows_4(self):
        self.assertEqual(self.game.bulls(self.number_4),
                         self.answer_4)

    def test_cows_0_bulls_0(self):
        self.assertEqual(self.game.bulls(self.number_5),
                         self.answer_5)

    def test_cows_2_bulls_1(self):
        self.assertEqual(self.game.bulls(self.number_6),
                         self.answer_6)

    @expectedFailure
    def test_invalid_answer_1(self):
        self.assertEqual(self.game.bulls(self.number_4),
                         self.answer_1)

    @expectedFailure
    def test_invalid_answer_2(self):
        self.assertEqual(self.game.bulls(self.number_6),
                         self.answer_3)
